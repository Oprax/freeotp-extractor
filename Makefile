.PHONY: build run test testdev clean install upload

clean:
	rm -rf ./*.egg-info
	rm -rf ./dist
	rm -rf ./bin
	rm -rf ./build
	rm -rf ./site-packages

build: clean
	pipenv lock -r > requirements.txt
	pipenv run pip install -r requirements.txt --target site-packages/
	# cp README.md site-packages/
	cp -a ./freeotp_extractor ./site-packages/
	mkdir -p bin
	pipenv run shiv --site-packages site-packages --compressed -p "/usr/bin/env python3" -o ./bin/freeotp_extractor.pyz -e freeotp_extractor.main

run: build
	pipenv run python ./bin/freeotp_extractor.pyz -v

test:
	PYTHONPATH="${PYTHONPATH}:${PWD}/src" pipenv run pytest --cov=freeotp_extractor

testdev:
	PYTHONPATH="${PYTHONPATH}:${PWD}/src" pipenv run pytest -s -x

install: clean
	pipenv run python setup.py install

upload: clean
	pipenv run python setup.py sdist bdist_wheel
	pipenv run twine upload ./dist/*
