# FreeOTP tokens extractor

<!--
[![coverage report](https://gitlab.com/Oprax/freeotp-extractor/badges/master/coverage.svg)](https://gitlab.com/Oprax/freeotp-extractor/commits/master)
[![Documentation Status](https://readthedocs.org/projects/freeotp-extractor/badge/?version=latest)](https://freeotp-extractor.readthedocs.io/en/latest/?badge=latest) -->
[![pipeline status](https://gitlab.com/Oprax/freeotp-extractor/badges/master/pipeline.svg)](https://gitlab.com/Oprax/freeotp-extractor/commits/master)
[![PyPI - License](https://img.shields.io/pypi/l/freeotp-extractor.svg)](https://gitlab.com/Oprax/freeotp-extractor/blob/master/LICENSE)
[![PyPI](https://img.shields.io/pypi/v/freeotp-extractor.svg)](https://pypi.org/project/freeotp-extractor/)
[![PyPI - Python Version](https://img.shields.io/pypi/pyversions/freeotp-extractor.svg)](https://pypi.org/project/freeotp-extractor/)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/ambv/black)

## Backing up FreeOTP

Using [adb](https://developer.android.com/studio/command-line/adb.html), [create a backup](https://androidquest.wordpress.com/2014/09/18/backup-applications-on-android-phone-with-adb/) of the app using the following command:

```sh
adb backup -f freeotp-backup.ab -apk org.fedorahosted.freeotp
```

[org.fedorahosted.freeotp](https://play.google.com/store/apps/details?id=org.fedorahosted.freeotp) is the app ID for FreeOTP.

This will ask, on the phone, for a password to encrypt the backup. Proceed with a password.

## Manually extracting the backup

The backups are some form of encrypted tar file. [Android Backup Extractor](https://github.com/nelenkov/android-backup-extractor) can decrypt them.

It's available on the AUR as [android-backup-extractor-git](https://aur.archlinux.org/packages/android-backup-extractor-git/).

Use it like so (this command will ask you for the password you just set to decrypt it):

```sh
abe unpack freeotp-backup.ab freeotp-backup.tar
```

Then extract the generated tar file:

```shell
$ tar xvf freeotp-backup.tar
apps/org.fedorahosted.freeotp/_manifest
apps/org.fedorahosted.freeotp/sp/tokens.xml
```

We don't care about the manifest file, so let's look at `apps/org.fedorahosted.freeotp/sp/tokens.xml`.

## Extract tokens

First, download [`freeotp_extractor.pyz`](https://gitlab.com/Oprax/freeotp-extractor/-/jobs/artifacts/master/raw/bin/freeotp_extractor.pyz?job=pyz) (or install it from [PyPi](https://pypi.org/project/freeotp-extractor/) with `pip`), then you can run `./freeotp_extractor.pyz -h` :

```
usage: freeotp_extractor.pyz [-h] [-v] [-o OUTPUT] [-q {term,svg,eps}] input

Extract token from FreeOTP

positional arguments:
  input                 File containing XML with tokens (usually 'tokens.xml')

optional arguments:
  -h, --help            show this help message and exit
  -v, --version         show program's version number and exit
  -o OUTPUT, --output OUTPUT
                        Give the output file for save tokens
  -q {term,svg,eps}, --qrcode {term,svg,eps}
                        Use a JSON input to recreate QRcode for each issuer.
                        Use 'term' to display directly to the terminal, 'svg'
                        and 'eps' output the qrcode into a file
```

To just output tokens in the termnal :
```sh
./freeotp_extractor.pyz apps/org.fedorahosted.freeotp/sp/tokens.xml
```

It will output something like :
```
Dropbox:example@gmail.com: BQ4F6XX3QOFEXQY5SNFPJZW3
gitlab.com:example@gmail.com: 4FBTY2GE3VK7BMFBFOE3X7CR
Google:example@gmail.com: RK6MVRZCQXFBUMGBKZBF5CAA
```

Or you can pass a `output` parameter to save it into a file :
```sh
./freeotp_extractor.pyz --output tokens.json apps/org.fedorahosted.freeotp/sp/tokens.xml
```

`tokens.json`:
```json
{
  "Dropbox:example@gmail.com":{
    "secret":"BQ4F6XX3QOFEXQY5SNFPJZW3",
    "issuer":"Dropbox"
  },
  "gitlab.com:example@gmail.com":{
    "secret":"4FBTY2GE3VK7BMFBFOE3X7CR",
    "issuer":"Gitlab"
  },
  "Google:example@gmail.com":{
    "secret":"RK6MVRZCQXFBUMGBKZBF5CAA",
    "issuer":"Google"
  }
}
```

## Recreate QRcode

With the JSON file (i.e.: `tokens.json`) you can recreate QRcode to scan from an application.
To output it directly to the terminal :
```sh
./freeotp_extractor.pyz tokens.json -q term
```

Or if you wan to save it into files :
```sh
mkdir -p ./qrcode
./freeotp_extractor.pyz tokens.json -q svg -o ./qrcode
```